#
# Copyright (C) 2015 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_ABI2 :=
TARGET_CPU_VARIANT := cortex-a53
TARGET_CPU_SMP := true

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv7-a-neon
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_2ND_CPU_VARIANT := cortex-a53

BOARD_OVERRIDE_RS_CPU_VARIANT_32 := cortex-a53
BOARD_OVERRIDE_RS_CPU_VARIANT_64 := cortex-a53

# Disable emulator for "make dist" until there is a 64-bit qemu kernel
BUILD_EMULATOR := false

TARGET_BOARD_PLATFORM := s5p6818

TARGET_USES_64_BIT_BCMDHD := true
TARGET_USES_64_BIT_BINDER := true

TARGET_BOOTLOADER_BOARD_NAME := nanopi3
TARGET_BOARD_INFO_FILE := device/friendlyelec/nanopi3/board-info.txt

ENABLE_CPUSETS := true
ENABLE_SCHEDBOOST := true

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL     := true
TARGET_NO_RADIOIMAGE := true
TARGET_NO_RECOVERY   := false
TARGET_BOOTLOADER_IS_2ND := false

TARGET_KERNEL_IMAGE  := Image

# recovery
TARGET_RECOVERY_PIXEL_FORMAT := RGBX_8888
TARGET_RECOVERY_UPDATER_LIBS := librecovery_updater_nexell
TARGET_RECOVERY_FSTAB := device/friendlyelec/nanopi3/recovery.fstab
TARGET_RELEASETOOLS_EXTENSIONS := device/friendlyelec/nanopi3
BOARD_USES_FULL_RECOVERY_IMAGE := true

BOARD_USES_GENERIC_AUDIO := false
BOARD_USES_ALSA_AUDIO := false
BOARD_USES_AUDIO_N := true

BOARD_EGL_CFG := device/friendlyelec/nanopi3/egl.cfg
USE_OPENGL_RENDERER := true
TARGET_USES_ION := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3

# see surfaceflinger
MAX_VIRTUAL_DISPLAY_DIMENSION := 2048

# Cache size limits
MAX_EGL_CACHE_KEY_SIZE := 12*1024
MAX_EGL_CACHE_SIZE := 2048*1024

# gralloc
BOARD_GRALLOC_ALIGN_FACTOR := 128

# Enable dex-preoptimization to speed up first boot sequence
ifeq ($(HOST_OS),linux)
  ifneq ($(TARGET_BUILD_VARIANT),eng)
    ifeq ($(WITH_DEXPREOPT),)
      WITH_DEXPREOPT := true
      WITH_DEXPREOPT_PIC := true
    endif
  endif
endif

# touch
BOARD_USES_TSLIB := true

BOARD_CHARGER_ENABLE_SUSPEND := false

# sepolicy
BOARD_SEPOLICY_DIRS += \
	device/friendlyelec/nanopi3/sepolicy

# certificate
include vendor/friendlyelec/nanopi3/security.mk

# filesystems
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 67108864
BOARD_BOOTIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2147483648
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_CACHEIMAGE_PARTITION_SIZE := 536870912
BOARD_USERDATAIMAGE_PARTITION_SIZE := 4920246272
BOARD_FLASH_BLOCK_SIZE := 131072

TARGET_USES_AOSP := true

USE_CLANG_PLATFORM_BUILD := true

# FFMPEG
ifneq ($(TARGET_ARCH),arm64)
-include device/friendlyelec/common/ffmpeg.mk
endif

# wifi and bluetooth
-include device/friendlyelec/common/wifi_bt_config.mk
-include device/friendlyelec/common/wifi_bt.mk

# Camera
BOARD_CAMERA_NUM := 0
BOARD_CAMERA_USE_ZOOM := false
BOARD_CAMERA_SUPPORT_SCALING := false

BOARD_CAMERA_BACK_DEVICE := "6"
BOARD_CAMERA_BACK_ORIENTATION := "0"
# Interlaced Mode
# default - Progressive
# Progressive - 0, Interlaced MIPI - 1, Interlaced Parallel - 2
BOARD_CAMERA_BACK_INTERLACED := "0"
# Clipper Only Mode - not use decimator for recording
BOARD_CAMERA_BACK_COPY_MODE := "1"

BOARD_NEXELL_LIGHTS := false

# Use the non-open-source parts, if they're present
-include vendor/quectel/ec20/BoardConfigPartial.mk
