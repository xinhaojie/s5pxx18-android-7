#
# Copyright (C) 2015 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_COPY_FILES += \
	device/friendlyelec/common/init.common.rc:root/init.common.rc \
	$(LOCAL_PATH)/init.nanopi3.rc:root/init.nanopi3.rc \
	$(LOCAL_PATH)/init.nanopi3.usb.rc:root/init.nanopi3.usb.rc \
	$(LOCAL_PATH)/fstab.nanopi3:root/fstab.nanopi3 \
	$(LOCAL_PATH)/fstab.nanopi3.emmc:root/fstab.nanopi3.emmc \
	$(LOCAL_PATH)/ueventd.nanopi3.rc:root/ueventd.nanopi3.rc \
	$(LOCAL_PATH)/init.recovery.nanopi3.rc:root/init.recovery.nanopi3.rc

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/boot/bl1-mmcboot.bin:bl1-mmcboot.bin \
	$(LOCAL_PATH)/boot/fip-loader.img:fip-loader.img \
	$(LOCAL_PATH)/boot/fip-secure.img:fip-secure.img \
	$(LOCAL_PATH)/boot/fip-nonsecure.img:fip-nonsecure.img \
	$(LOCAL_PATH)/boot/env.conf:env.conf \
	$(LOCAL_PATH)/boot/partmap.txt:partmap.txt

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/boot/Image:boot/Image \
	$(LOCAL_PATH)/boot/s5p6818-nanopi3-rev01.dtb:boot/s5p6818-nanopi3-rev01.dtb \
	$(LOCAL_PATH)/boot/s5p6818-nanopi3-rev02.dtb:boot/s5p6818-nanopi3-rev02.dtb \
	$(LOCAL_PATH)/boot/s5p6818-nanopi3-rev03.dtb:boot/s5p6818-nanopi3-rev03.dtb \
	$(LOCAL_PATH)/boot/s5p6818-nanopi3-rev05.dtb:boot/s5p6818-nanopi3-rev05.dtb \
	$(LOCAL_PATH)/boot/s5p6818-nanopi3-rev07.dtb:boot/s5p6818-nanopi3-rev07.dtb

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/boot/logo.bmp:boot/logo.bmp \
	$(LOCAL_PATH)/boot/battery.bmp:boot/battery.bmp \
	$(LOCAL_PATH)/boot/update.bmp:boot/update.bmp

# Power configuration file
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.power.sh:system/bin/init.power.sh

PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:system/etc/media_codecs_google_audio.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:system/etc/media_codecs_google_telephony.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:system/etc/media_codecs_google_video.xml

# audio
USE_XML_AUDIO_POLICY_CONF := 1

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/audio/tiny_hw.nanopi3.xml:system/etc/tiny_hw.nanopi3.xml \
	$(LOCAL_PATH)/audio/audio_policy.conf:system/etc/audio_policy.conf

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/audio/mixer_paths.xml:system/etc/mixer_paths.xml \
	$(LOCAL_PATH)/audio/audio_policy_configuration.xml:system/etc/audio_policy_configuration.xml \
	$(LOCAL_PATH)/audio/audio_policy_volumes.xml:system/etc/audio_policy_volumes.xml \
	$(LOCAL_PATH)/audio/a2dp_audio_policy_configuration.xml:system/etc/a2dp_audio_policy_configuration.xml \
	$(LOCAL_PATH)/audio/usb_audio_policy_configuration.xml:system/etc/usb_audio_policy_configuration.xml \
	$(LOCAL_PATH)/audio/r_submix_audio_policy_configuration.xml:system/etc/r_submix_audio_policy_configuration.xml \
	$(LOCAL_PATH)/audio/default_volume_tables.xml:system/etc/default_volume_tables.xml

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/media_codecs.xml:system/etc/media_codecs.xml \
	$(LOCAL_PATH)/media_profiles.xml:system/etc/media_profiles.xml

# input
PRODUCT_COPY_FILES += \
	device/friendlyelec/common/ft5x0x_ts.idc:system/usr/idc/ft5x0x_ts.idc \
	device/friendlyelec/common/it7260_ts.idc:system/usr/idc/it7260_ts.idc \
	device/friendlyelec/common/fa_ts_input.idc:system/usr/idc/fa_ts_input.idc \
	device/friendlyelec/common/gpio_keys.kl:system/usr/keylayout/gpio_keys.kl \
	device/friendlyelec/common/gpio_keys.kcm:system/usr/keychars/gpio_keys.kcm

# These are the hardware-specific features
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
	frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
	frameworks/native/data/etc/android.hardware.camera.autofocus.xml:system/etc/permissions/android.hardware.camera.autofocus.xml \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
	frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
	frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
	frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:system/etc/permissions/android.hardware.audio.pro.xml \
	frameworks/native/data/etc/android.hardware.telephony.cdma.xml:system/etc/permissions/android.hardware.telephony.cdma.xml \
	frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml

# Screen size is "normal", density is "hdpi"
PRODUCT_AAPT_CONFIG := normal large xlarge mdpi hdpi xhdpi
PRODUCT_AAPT_PREF_CONFIG := hdpi
PRODUCT_AAPT_PREBUILT_DPI := hdpi mdpi xhdpi

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_TAGS += dalvik.gc.type-precise

# OpenGL ES API version: 2.0
PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=131072

PRODUCT_PROPERTY_OVERRIDES += \
	ro.hwui.texture_cache_size=56 \
	ro.hwui.layer_cache_size=32 \
	ro.hwui.r_buffer_cache_size=8 \
	ro.hwui.path_cache_size=16 \
	ro.hwui.gradient_cache_size=1 \
	ro.hwui.drop_shadow_cache_size=6 \
	ro.hwui.texture_cache_flushrate=0.4 \
	ro.hwui.text_small_cache_width=1024 \
	ro.hwui.text_small_cache_height=1024 \
	ro.hwui.text_large_cache_width=2048 \
	ro.hwui.text_large_cache_height=1024

PRODUCT_PROPERTY_OVERRIDES += \
	persist.demo.hdmirotates=true

# libion needed by gralloc, ogl
PRODUCT_PACKAGES += libion iontest

PRODUCT_PACKAGES += \
	libdrm \
	libGLES_mali

# HAL
PRODUCT_PACKAGES += \
	gralloc.nanopi3 \
	hwcomposer.nanopi3 \
	audio.primary.nanopi3 \
	memtrack.nanopi3 \
	lights.nanopi3 \
	camera.nanopi3

PRODUCT_PACKAGES += \
	audio.a2dp.default \
	audio.usb.default \
	audio.r_submix.default \
	libaudio-resampler

# tinyalsa
PRODUCT_PACKAGES += \
	libtinyalsa \
	tinyplay \
	tinycap \
	tinymix \
	tinypcminfo

# For android_filesystem_config.h
PRODUCT_PACKAGES += fs_config_files

DEVICE_PACKAGE_OVERLAYS := $(LOCAL_PATH)/overlay

# omx
PRODUCT_PACKAGES += \
	libstagefrighthw \
	libnx_video_api \
	libNX_OMX_VIDEO_DECODER \
	libNX_OMX_VIDEO_ENCODER \
	libNX_OMX_Base \
	libNX_OMX_Core \
	libNX_OMX_Common

# Enable AAC 5.1 output
PRODUCT_PROPERTY_OVERRIDES += \
	media.aac_51_output_enabled=true

# ffmpeg
ifeq ($(BUILD_32BIT_ONLY),true)
-include device/friendlyelec/common/ffmpeg.mk
endif

ifeq ($(EN_FFMPEG_AUDIO_DEC),true)
PRODUCT_PACKAGES += \
	libNX_OMX_AUDIO_DECODER_FFMPEG
endif

ifeq ($(EN_FFMPEG_EXTRACTOR),true)
PRODUCT_PACKAGES += \
	libNX_FFMpegExtractor

PRODUCT_COPY_FILES += \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavcodec.so:system/lib/libavcodec.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavcodec.so.55:system/lib/libavcodec.so.55 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavcodec.so.55.39.101:system/lib/libavcodec.so.55.39.101 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavdevice.so:system/lib/libavdevice.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavdevice.so.55:system/lib/libavdevice.so.55 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavdevice.so.55.5.100:system/lib/libavdevice.so.55.5.100 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavfilter.so:system/lib/libavfilter.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavfilter.so.3:system/lib/libavfilter.so.3 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavfilter.so.3.90.100:system/lib/libavfilter.so.3.90.100 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavformat.so:system/lib/libavformat.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavformat.so.55:system/lib/libavformat.so.55 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavformat.so.55.19.104:system/lib/libavformat.so.55.19.104 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavresample.so:system/lib/libavresample.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavresample.so.1:system/lib/libavresample.so.1 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavresample.so.1.1.0:system/lib/libavresample.so.1.1.0 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavutil.so:system/lib/libavutil.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavutil.so.52:system/lib/libavutil.so.52 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libavutil.so.52.48.101:system/lib/libavutil.so.52.48.101 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libswresample.so:system/lib/libswresample.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libswresample.so.0:system/lib/libswresample.so.0 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libswresample.so.0.17.104:system/lib/libswresample.so.0.17.104 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libswscale.so:system/lib/libswscale.so \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libswscale.so.2:system/lib/libswscale.so.2 \
	hardware/nexell/s5pxx18/omx/codec/ffmpeg/32bit/libs/libswscale.so.2.5.101:system/lib/libswscale.so.2.5.101

endif ### EN_FFMPEG_EXTRACTOR ###

# Recovery
PRODUCT_PACKAGES += \
	librecovery_updater_nexell

# tslib
PRODUCT_COPY_FILES += \
	external/tslib/ts.conf:system/etc/ts.conf \
	$(LOCAL_PATH)/pointercal:system/etc/pointercal

PRODUCT_PACKAGES += \
	libtslib \
	inputraw \
	pthres \
	dejitter \
	linear \
	tscalib

# Connectivity
-include device/friendlyelec/common/wifi_bt_config.mk
$(call inherit-product-if-exists, vendor/broadcom/ap6212/device-partial.mk)
$(call inherit-product-if-exists, vendor/friendlyelec/nanopi3/device-partial.mk)

PRODUCT_PACKAGES += \
	libwpa_client \
	hostapd \
	dhcpcd.conf \
	wpa_supplicant \
	wpa_supplicant.conf

ifeq ($(BOARD_WIFI_VENDOR),realtek)
PRODUCT_PACKAGES += \
	rtw_fwloader

PRODUCT_COPY_FILES += \
	hardware/realtek/wlan/driver/rtl8188eus/wlan.ko:system/vendor/realtek/wlan.ko
endif

PRODUCT_PROPERTY_OVERRIDES += \
	wifi.interface=wlan0

ifeq ($(BOARD_BLUETOOTH_VENDOR),false)
PRODUCT_PROPERTY_OVERRIDES += \
	config.disable_bluetooth=true
endif

# Live Wallpapers
PRODUCT_PACKAGES += \
	WallpaperPicker \
	LiveWallpapersPicker \
	librs_jni

# Vold
PRODUCT_PACKAGES += \
	fsck.exfat \
	mkfs.exfat \
	mount.exfat \
	fsck.ntfs \
	mkfs.ntfs \
	mount.ntfs

# increase dex2oat threads to improve booting time
PRODUCT_PROPERTY_OVERRIDES += \
	dalvik.vm.boot-dex2oat-threads=8 \
	dalvik.vm.dex2oat-threads=8 \
	dalvik.vm.image-dex2oat-threads=8

# setup dalvik vm configs.
PRODUCT_PROPERTY_OVERRIDES += \
	dalvik.vm.heapstartsize=16m \
	dalvik.vm.heapgrowthlimit=192m \
	dalvik.vm.heapsize=512m \
	dalvik.vm.heaptargetutilization=0.75 \
	dalvik.vm.heapminfree=512k \
	dalvik.vm.heapmaxfree=8m

# set default none for usb
PRODUCT_PROPERTY_OVERRIDES += \
	persist.sys.usb.config=none

# skip boot jars check
SKIP_BOOT_JARS_CHECK := true

# vendor/3rdparty support
$(call inherit-product-if-exists, vendor/friendlyelec/apps/device-partial.mk)

# google gms
ifeq ($(GAPPS),yes)
$(call inherit-product-if-exists, vendor/google/gapps/device-partial.mk)
endif

# nexell carlife support
-include device/friendlyelec/common/carlife.mk

# 3G/4G dongle
-include vendor/quectel/ec20/BoardConfigPartial.mk

EC20_LIB_ARCH := arm64
$(call inherit-product-if-exists, vendor/quectel/ec20/device-partial.mk)

ifeq ($(BOARD_HAVE_DONGLE),true)
PRODUCT_PROPERTY_OVERRIDES += \
	keyguard.no_require_sim=true \
	ro.com.android.dataroaming=true \
	ro.boot.noril=false

else
PRODUCT_PROPERTY_OVERRIDES += ro.boot.noril=true
endif
